#!/bin/bash
# ScriptName: port_scanner
# Date: 2nd June 2023
# Author: Sanju P Debnath

# Remote host address
remote_host='ftp.gnu.org'

# Port range to scan
port_range=({20..30})

# Loop script to scan
for port in ${port_range[@]}
do
(timeout -s9 2 echo > /dev/tcp/"$remote_host"/"$port") 2> /dev/null && echo "Open port $port"
done
