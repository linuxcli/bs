#!/bin/bash
#Author: Sanju P Debnath
STATUS=`service tomcat status | grep not`
SUB="Tomcat service Notification"
RECV="user@localhost.local"
if [ -n "$STATUS" ] ;
then
service tomcat start 
else
exit
fi

## Mail Notification
if [ -z "$STATUS" ];
then
echo "Successfully started Tomcat service" | mail -s $SUB $RECV
else
exit
fi
