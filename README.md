# apt
Auto print text: This script print each character on screen of input text file (ASCII format).

---
---
<br/>
<br/>

# bm
Version    : 0.1

This is a bind mount script which require while chrooting the directory
with require objects to be use in chroot environment to fork
-h  : Help menu
-u  : Unmount bind mounts
-m  : Mount bind devices

Example:
 bm -m /target/chroot/directory

---
---
<br/>
<br/>

# changehostname
Change hostname also update host file.

---
---
<br/>
<br/>

# ci
Script to monitor internet connectivity threshold within second.
```
curl -kL linuxcli.in/s/ci
```

---
---
<br/>
<br/>

# get-info
Version    : 0.1

 - get-info is a script to get details about available free disk space
   and network status of remote machine.

 - This script require SSh.
   - At first configure communication keys by adding switch --add and
     address of target machine.

     `get-info.sh --add <username>@<IP Address>`



 * To get-info of remote machine use below example command.

   `get-info.sh check <username>@<IP Address>`


 * Simple command. To make it simple you can ignore command if the logged
   in user name exist on remote machine with enabled shell then you can
   use below simple command.

   `get-info.sh check <IP Address>`

    Or

   `get-info.sh check <Hostname>`

   If hostname is able to resolve in IP.


For more about license use command

  `get-info.sh license`


<br>


Tip: For quick result disable UseDNS `UseDNS no` option on remote SSh configuration (/etc/ssh/sshd_config).

---
---
<br/>
<br/>

# pingscan
Version: 0.1
Ping scan is a script to find IP addresses online in local network. To work this script ICMP protocol needs to be enable or unblock in firewall of remote machines.


## Getting started

Below example to use pingscan script.
<pre>
$ ps.sh  [IP Address] Last octet of [Starting IP] [End IP]
</pre>
```
$ ./ps.sh  192.168.16.1  20  80
```
<pre>
In the above example first argument is the Network IP address (192.168.16.1),
second argument is last octet of starting IP address (20) and the third argument is
last octect of ending IP address (80) of the range to scan.

For simple manual execute script with out any argument.
</pre>

---
---
<br/>
<br/>

# pkg-install
Interactive YUM or APT-GET package installation.

---
---
<br/>
<br/>

# service-status-notify
Notify service status script for sysvinit service manager.

---
---
<br/>
<br/>

# tmpuser

#### To use this script root user privileges is require
Follow below mention steps to start with temp user script.

#### Make executable script.
chmod +x tmpusr.sh

#### To see syntax.
./tmpusr.sh help   

#### To setup temp "user" script.
sudo ./tmpusr.sh install

#### To start temp user profile.
sudo ./tmpusr.sh start

