#!/bin/bash
# Author: Sanju P Debnath
# Description: Create temporary user.
# Copyright License : MIT License
USR=root
WHO=`echo $USER`
HELP="\r Usage: tmpusr [OPTIONS]\n

Author: Sanju P Debnath\n
Description: Create temporary user.\n
\n
 *Options*\n
help : To see syntax.\n
lic : To read License Agreement.\n
install : To install tmpusr.\n
start : To start tmp user profile.\n
\n
Note: To login with user \"user\" default password is \"pass\".\n
"
if [ "$1" = "help" ];then
echo -e $HELP
elif [ "$1" = "lic" ];then
if [ -e LICENSE ];then
less LICENSE
else 
echo -e "License file missing from current directory"
fi
elif [ "$WHO" != "$USR" ]; then
echo -e " Require root privileges unable to execute\n Run with sudo"
exit 0
elif [ "$1" = "install" ];then
mkdir -vp /etc/tmpusr/skel/ && echo "Created tmpuser skel directory at path /etc/tmpusr/skel"
useradd -md /tmp/user/ -s `which bash` user
echo "user:pass"|chpasswd && echo "Created username \"user\" with password \"pass\" "
cp ./tmpusr.sh /usr/bin/tmpusr
chmod +x /usr/bin/tmpusr
echo "/usr/bin/tmpusr start" >> /etc/rc.local
echo -e "Install tmpuser script successfully"
elif [ "$1" = "start" ];then
cd /tmp
mkdir user
cp -rT /etc/skel/ user
cp -rT /etc/tmpusr/skel/ user
chown -R user:user user
chmod -R 700 user
else
exit 0
fi
