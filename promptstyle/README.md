# promptstyle
Decorate your bash shell prompt

Add below lines in your profile file .bashrc to customized and colorize your prompt.<br/>

---
`style1:`<br/>
`In this bash prompt *node0* is a hostname and *linuxcli.in* is the domain name and last is the current working directory.`<br/>
```
curl -kL linuxcli.in/ps/1 >> ~/.bashrc
source ~/.bashrc
```
<br/>

`style2:`<br/>
`In this bash prompt *root* is the user followed by separator pipe | *node0.linuxcli.in* is the FQDN and last is the current working directory.`<br/>
```
curl -kL linuxcli.in/ps/2 >> ~/.bashrc
source ~/.bashrc
```
<br/>

`style3:`<br/>
`In this bash prompt *<0>* is the current tty number *[01:02:03]* is current time *|-Sat01/Jan-|* current date.`<br/>
```
curl -kL linuxcli.in/ps/3 >> ~/.bashrc
source ~/.bashrc
```
