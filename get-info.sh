#!/bin/bash
# Author: Sanju P Debnath
# Version: 0.1
# License: MIT License
# ScriptName: get-info

AUTHOR="Sanju P Debnath"
VER="0.1"
LIC="MIT License"
SCRPTNM="get-info"

ROOTDIR="$HOME/.mon-tools"
PRIKEY="$ROOTDIR/comkey"
PUBKEY="$ROOTDIR/comkey.pub"

shopt -s expand_aliases
alias ssh='ssh -i "$PRIKEY"'

mkdir "$ROOTDIR" 2> /dev/null

# -----functions-start-from-here----- #


help () {
printf "
 ScriptName : $SCRPTNM
 Version    : $VER
 License    : $LIC
 
 - get-info is a script to get details about available free disk space
   and network status of remote machine.

 - This script require SSh.
   - At first configure communication keys by adding switch --add and
     address of target machine.

     $ get-info --add <username>@<IP Address>



 * To get-info of remote machine use below example command.

   $ get-info check <username>@<IP Address>


 * Simple command. To make it simple you can ignore command if the logged
   in user name exist on remote machine with enabled shell then you can 
   use below simple command. 

   $ get-info check <IP Address>

	Or

   $ get-info check <Hostname>

   If hostname is able to resolve in IP.


For more about license use command 

  $ get-info license
\n"
  }

license () {
printf "MIT License

Copyright (c) 2020 Sanju P Debnath

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.\n\n"
 }

create_key () {
printf "There are no keys for communication.\nCreating keys...\n\n"
echo ""|ssh-keygen -q -t rsa -E md5 -b 2048 -f "$PRIKEY" -P "" -C "Monitoring agent" >& /dev/null	&& 	\
printf  "Keys generated successfully.\n\n\
Run below commands to add machines for passwordless communication.\n\
------------------------------------------\n\
| get-info --add <username>@<ip_address> |\n\
------------------------------------------\n\
In above username is the SSh user and the ip_address is the address of target machine.\n\
Use \"help\" switch for more info.\nget-info help\n"							|| 	\
printf "Failed to create keys for communication.\n\
Kindly check user $USER home directory $HOME must have Write permission recursively.\n\
Use \"help\" switch for more info.\nget-info help\n"
}

sys () {
HOME="/tmp/mont/sys"
PT=$(awk '/MemTotal/{print $2}' $HOME/mem)
VT=$(awk '/SwapTotal/{print $2}' $HOME/mem)
PF=$(awk '/MemFree/{print $2}' $HOME/mem)
VF=$(awk '/SwapFree/{print $2}' $HOME/mem)

printf "
 Host ($(awk '{print $2}' $HOME/uname))
 Kernel ($(awk '{print $3}' $HOME/uname))
 Architecture ($(awk '{print $(NF-1)}' $HOME/uname))
 Processor ($(sed -n '/processor/p' $HOME/cpu|wc -l) core)
 Physical Memory ($((100*$PF/$PT))%% free)
 Virtual  Memory ($((100*$VF/$VT))%% free)
"

}


disk () {
HOME="/tmp/mont/disk"
EX_FS='squashfs|tmpfs'
DEV=$(cat $HOME/mounts |sed "/^\/dev/p;d"|awk '$3 !~ '"/$EX_FS/ {print}"|awk '{print $1}'|sort -r|uniq)

printf " Mount_Point %-4s Free_Space %-4s Free_Inode %-4s FileSystem
$(for A in $DEV
do 

DFHF="$HOME/dfh"
DFIF="$HOME/dfi"
DFHC="$(grep "$A" $DFHF)"
DFIC="$(grep "$A" $DFIF)"

if [ -n "$DFHC" ]
then
DFH="$DFHC"
DFI="$DFIC"
else
continue
fi

MP="$(echo "$DFH"|awk '{print $NF}')"
FSP="$((100-$(echo "$DFH"|awk '{print $6}'|tr -d "%")))%%"
FIP="$((100-$(echo "$DFI"|awk '{print $5}'|tr -d "%")))%%"
FSY="$(echo "$DFH" |awk '{print $2}')"
	
 if [ "${#MP}" -ge "15" ]
 then 
 MPP="$(echo "$MP"|cut -c-10)%6s"
 elif [ "15" -gt "${#MP}" ]
 then 
 MPP="$(echo "$MP %$((15-${#MP}))s")"
  fi
 
 if [ "${#FSP}" -ge "10" ]
 then 
 FSPP="$(echo  "$FSP"|cut -c-10)"
 elif [ "10" -gt "${#FSP}" ]
 then
 FSPP="$(echo "$FSP %$((10-${#FSP}))s")"
 fi
 
 if [ "${#FIP}" -ge "10" ]
 then 
 FIPP="$(echo  "$FIP"|cut -c-10)"
 elif [ "10" -gt "${#FIP}" ]
 then
 FIPP="$(echo "$FIP %$((10-${#FIP}))s")"
 fi
 
 if [ "${#FSY}" -ge "10" ]
 then 
 FSYP="$(echo  "$FSY"|cut -c-10)"
 elif [ "10" -gt "${#FSY}" ]
 then
 FSYP="$(echo "$FSY %$((10-${#FSY}))s")"
 fi

 echo "  $MPP %4s $FSPP %4s $FIPP %4s $FSYP "
done)\n"
}

network () {
HOME="/tmp/mont/network"
printf " Device %-16s Mac Address %-6s IPv4 %-16s Error(RecP)  Error(SntP)  Drop(RecP)  Drop(SntP)
$(for A in $(cat $HOME/dev)
do

 MAC="$(cat $HOME/$A.dev|awk '/ether /{print $2}')"
 IPV4="$(cat $HOME/$A.dev|awk '/inet /{print $2}')"
 
 if [ "${#A}" -ge "15" ]
 then 
 DEVP="$(echo "$A"|cut -c-10)%6s"
 elif [ "15" -gt "${#A}" ]
 then
 DEVP="$(echo "$A %$((15-"${#A}"))s")"
 fi
 
 if [ -z "$MAC" ]
 then 
 MAC="%1s Not Assigned %2s"
 fi
 
 if [ -z "$IPV4" ]
 then 
 IPV4="Offline"
 else
 IPV4="$(echo "$IPV4 %$((19-${#IPV4}))s")"
 fi

 ERL=$(sed -n '/errors/=' $HOME/$A.dev)
 ERLR=$(echo "$ERL"|sed '1p;d')
 ERLS=$(echo "$ERL"|sed '2p;d')
 ERR=$(awk '{print $3}' $HOME/$A.dev|sed "$(($ERLR+1)) p;d")
 ERS=$(awk '{print $3}' $HOME/$A.dev|sed "$(($ERLS+1)) p;d")
 
 DRL=$(sed -n '/dropped/=' $HOME/$A.dev)
 DRLR=$(echo "$DRL"|sed '1p;d')
 DRLS=$(echo "$DRL"|sed '2p;d')
 DRR=$(awk '{print $4}' $HOME/$A.dev|sed "$(($DRLR+1)) p;d")
 DRS=$(awk '{print $4}' $HOME/$A.dev|sed "$(($DRLS+1)) p;d")

 if [ "${#ERR}" -ge "10" ]
 then 
 RecErr="$(echo "$ERR"|cut -c-10)"
 elif [ "10" -gt "${#ERR}" ]
 then
 RecErr=$(echo "${ERR} %$((10-${#ERR}))s")
 fi

 if [ "${#ERS}" -ge "10" ]
 then 
 SntErr="$(echo "$ERS"|cut -c-10)"
 elif [ "10" -gt "${#ERS}" ]
 then
 SntErr=$(echo "${ERS} %$((10-${#ERS}))s")
 fi

 if [ "${#DRR}" -ge "10" ]
 then 
 DrpRec="$(echo "$DRR"|cut -c-10)"
 elif [ "10" -gt "${#DRR}" ]
 then
 DrpRec=$(echo "${DRR} %$((10-${#DRR}))s")
 fi

 if [ "${#DRS}" -ge "10" ]
 then 
 DrpSnt="$(echo "$DRS"|cut -c-10)"
 elif [ "10" -gt "${#DRS}" ]
 then
 DrpSnt=$(echo "${DRS} %$((10-${#DRS}))s")
 fi

 echo " $DEVP %3s $MAC %3s $IPV4  $RecErr  $SntErr  $DrpRec  $DrpSnt %3s"
done)\n"
}

SSHADD=$2
check () {
rm -rf /tmp/mont &> /dev/null

# system info
DIR="/tmp/mont/sys"
mkdir -p "$DIR"
ssh "$SSHADD" "uname -a" > $DIR/uname
ssh "$SSHADD" "cat /proc/meminfo" > $DIR/mem
ssh "$SSHADD" "cat /proc/cpuinfo" > $DIR/cpu
sys
printf "\n"

# Disk info
echo "Gathering Storage info"
DIR="/tmp/mont/disk"
mkdir -p "$DIR"
ssh "$SSHADD" "cat /proc/mounts" > $DIR/mounts
ssh "$SSHADD" "df -hT" > $DIR/dfh
ssh "$SSHADD" "df -i" > $DIR/dfi
disk
printf "\n"

# Network info
echo "Gathering Network info"
DIR="/tmp/mont/network"
mkdir -p "$DIR"
ssh "$SSHADD" "/usr/sbin/ip -br addr" > $DIR/dev_ori 2> /dev/null || ssh "$SSHADD" "/sbin/ip -br addr" > $DIR/dev_ori 2> /dev/null
awk '$1 ^ /lo/{print $1}'  $DIR/dev_ori  >  $DIR/dev
for A in $(cat $DIR/dev)
do
ssh "$SSHADD" "/usr/sbin/ip -s addr show $A" > $DIR/$A.dev 2> /dev/null || ssh "$SSHADD" "/sbin/ip -s addr show $A" > $DIR/$A.dev 2> /dev/null
done
network
}

# -----functions-end-here----- #

CHKPRIKEY="$(file $PRIKEY|grep "RSA private")"
CHKPUBKEY="$(file $PUBKEY|grep "RSA public")"

	if [ "$1" == "-h" ]
	then
 help
	elif [ "$1" == "help" ]
	then
 help
	elif [ "$1" == "--help" ]
	then
 help
	elif [ "$1" == "h" ]
	then
 help
	elif [ "$1" == "license" ] 
	then
 license
elif [ -n "$CHKPRIKEY" ]
then
  if [ -n "$CHKPUBKEY" ]
  then
    if [ "$1" == "--add" ]
    then
ssh-copy-id -f -i "$PUBKEY" $2 &> /dev/null || printf "File ~/.ssh/authorized_keys of target user must have Write permission.\n"
    elif [ "$1" == "check" ]
    then
check
 printf "\n"
    else
echo "Wrong input, please check the switch"
    fi
  else
ssh-keygen -f $PRIKEY -y > $PUBKEY && sed "$ s/$/ Monitoring agent/" $PUBKEY
  fi
else
create_key
fi
