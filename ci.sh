#!/bin/bash
#Author: Sanju Debnath
#Script Name: checkInternet.sh
#Create Date: 29 Dec 2023
 CON="\e[36mOnline \e[0m"
DCON="\e[31mOffline\e[0m"
NUM=1
while [ "$NUM" -gt '0' ]
do
while :
 do
    if [ "$(curl --connect-timeout 0.5 -s linuxcli.in/chk)" = 'OK' ] 2> /dev/null
	then
		printf "\r\c"
		printf "\r$CON $NUM "
    else
		printf "\r\c"
		printf "\r$DCON $NUM "
    fi
    ((NUM++))
	sleep 0.5
	done
 done
